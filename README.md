Assignment 2
------------

- ppl.hard.peerb.pdf (file, main document of this project, please read for all details)
- ppl.hard.peerb.tex (file, source code for document of this project)
- question1.circ (file, design of circuit for question 1, logisim)
- question1.jpg (file, design of circuit for question 1, image jpg)
- question1table.jpg (file, design of truth table for question 1, image jpg)
- question3.circ (file, design of circuit for question 3, logisim)
- question3.jpg (file, design of circuit for question 3, image jpg)
